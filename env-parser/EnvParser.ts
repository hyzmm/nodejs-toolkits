import ValueParser from "./ValueParser";

enum Type {
    NUMBER,
    STRING,
    BOOLEAN,
}

interface OptionDescription {
    name: string;
    description: string;
    type: Type;
    defaultValue: string;
}

class EnvParser {
    private static _options: OptionDescription[];
    private static _result: Map<string, any>;

    /**
     * use this method like:
     * EnvParse.addOptions()
     *  (...)
     *  (...)
     *  ...
     *
     * @returns {<ValueType>(name: string, description: string, type: Type, defaultValue: ValueType) => <ValueType>(name: string, description: string, type: Type, defaultValue: ValueType) => any}
     */
    public static addOptions() {
        if (!this._options) {
            this._options = [];
            this._result = new Map();
        }

        return EnvParser.addOption;
    }

    public static help() {
        const names: string[] = EnvParser._options.map(i => i.name);
        const types: string[] = EnvParser._options.map(i => Type[i.type]);
        const dVals: string[] = EnvParser._options.map(i => i.defaultValue || "");
        const descs: string[] = EnvParser._options.map(i => i.description);

        const nameHeader: string = "name";
        const typeHeader: string = "type";
        const dValHeader: string = "default value";
        const descHeader: string = "description";

        const nameMaxLen: number = Math.max(
            2 + nameHeader.length,
            names.sort((a, b) => b.length - a.length).shift().length,
        );
        const typeMaxLen: number = Math.max(
            2 + typeHeader.length,
            types.sort((a, b) => b.length - a.length).shift().length,
        );
        const dValMaxLen: number = Math.max(
            2 + dValHeader.length,
            dVals.sort((a, b) => b.length - a.length).shift().length,
        );
        const descMaxLen: number = Math.max(
            2 + descHeader.length,
            descs.sort((a, b) => b.length - a.length).shift().length,
        );
        const lineMaxLen: number = nameMaxLen + typeMaxLen + dValMaxLen + descMaxLen + 3 * 2 + 4;

        let helpString: string = `${"".padEnd(lineMaxLen, "=")}
environment variables help message";
${"".padEnd(lineMaxLen, "-")}
`;

        helpString +=
            "|" +
            alignString(nameHeader, nameMaxLen) +
            "|" +
            alignString(typeHeader, typeMaxLen + 2) +
            "|" +
            alignString(dValHeader, dValMaxLen + 3) +
            "|" +
            alignString(descHeader, descMaxLen) +
            "|";
        helpString += `\n${"".padEnd(lineMaxLen, "-")}\n`;

        for (const option of EnvParser._options) {
            helpString +=
                option.name.padEnd(nameMaxLen) +
                " : " +
                Type[option.type].padEnd(typeMaxLen) +
                (option.defaultValue ? " = " + option.defaultValue : "").padEnd(dValMaxLen + 3) +
                "    " +
                option.description +
                "\n";
        }

        helpString += "".padEnd(lineMaxLen, "=") + "\n";
        console.log(helpString);
    }

    public static parseEnvironmentVariables() {
        console.log(`=====================================
parse environment variables
-------------------------------------`);

        for (const desc of EnvParser._options) {
            const name: string = desc.name;
            let value: any;

            if (name in process.env) {
                value = process.env[name];
            } else {
                if (desc.defaultValue) {
                    console.log(`env.${name} not set. Default to ${desc.defaultValue}`);
                    value = desc.defaultValue;
                } else {
                    console.log(`env.${name} must be set`);
                    process.exit(1);
                }
            }

            value = ValueParser.parse(value, desc.type);
            EnvParser._result.set(name, value);
        }

        console.log(
            `-------------------------------------
environment variables verified
======================================
`,
        );
    }

    public static print(): void {
        const names: string[] = EnvParser._options.map(i => i.name);
        const nameMaxLen: number = names.sort((a, b) => b.length - a.length).shift().length;

        console.log(
            `======================================
environment variables
-------------------------------------`,
        );

        for (const [k, v] of EnvParser._result.entries()) {
            console.log(`${k.padEnd(nameMaxLen)} : ${v}`);
        }
        console.log("======================================");
    }

    public static getValue<T>(name: string): T {
        return EnvParser._result.get(name);
    }

    private static addOption(name: string, desc: string, type: Type, defaultValue?: string) {
        EnvParser._options.push({
            name: name,
            description: desc,
            type: type,
            defaultValue: defaultValue,
        });
        return EnvParser.addOption;
    }
}

function alignString(str: string, len: number, fillChar: string = " "): string {
    return (
        "".padEnd(Math.floor((len - str.length) / 2), fillChar) +
        str +
        "".padEnd(Math.ceil((len - str.length) / 2), fillChar)
    );
}

export { EnvParser, Type };
