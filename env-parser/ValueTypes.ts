class NumberType {
    public static parse(value: any): number {
        return parseFloat(value);
    }
}

class StringType {
    public static parse(value: any): string {
        return value;
    }
}

class BooleanType {
    public static parse(value: any): boolean {
        return /yes|on|true|disabled/.test(value);
    }
}

export { NumberType, StringType, BooleanType };
