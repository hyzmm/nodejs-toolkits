import { readFileSync, watch } from "fs";
import { isAbsolute, resolve } from "path";

export class ConfigData {
    private static _configs: Map<number, any>;

    public static addDataFiles() {
        if (!ConfigData._configs) {
            ConfigData._configs = new Map();
        }

        return ConfigData.addDataFile;
    }

    public static get<T>(id: number): T {
        return ConfigData._configs.get(id);
    }

    private static addDataFile(id: number, path: string, watchFileChange: boolean = true) {
        if (!isAbsolute(path)) {
            path = resolve(process.cwd(), path);
        }
        ConfigData.loadDataFile(id, path);

        if (watchFileChange) {
            watch(path, (eventType, filename) => {
                if (filename) {
                    console.log(`${path} changed, reload it`);
                    ConfigData.loadDataFile(id, path);
                }
            });
        }

        return ConfigData.addDataFile;
    }

    private static loadDataFile(id: number, path: string): void {
        try {
            const fileContent: string = readFileSync(path).toString();
            const json: any = JSON.parse(fileContent);
            ConfigData._configs.set(id, json);
        } catch (e) {
            console.error(`failed to load and parse data file ${path}`);
        }
    }
}
